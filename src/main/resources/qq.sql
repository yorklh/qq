/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80031
Source Host           : localhost:3306
Source Database       : qq

Target Server Type    : MYSQL
Target Server Version : 80031
File Encoding         : 65001

Date: 2023-04-25 09:45:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for chat_records
-- ----------------------------
DROP TABLE IF EXISTS `chat_records`;
CREATE TABLE `chat_records` (
  `id` bigint NOT NULL COMMENT '聊天记录表',
  `sender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户名',
  `receiver` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户名',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '聊天内容',
  `sort` bigint DEFAULT NULL COMMENT '根据排序来决定前端输出顺序',
  `create_time` datetime DEFAULT NULL COMMENT '发送日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of chat_records
-- ----------------------------
INSERT INTO `chat_records` VALUES ('1650662812999639041', 'zhangsan', 'lisi', 'hello lisi', null, '2023-04-25 08:47:26');
INSERT INTO `chat_records` VALUES ('1650665942399205378', 'zhangsan', 'lisi', '你好李四', null, '2023-04-25 08:59:53');
INSERT INTO `chat_records` VALUES ('1650667829366226945', 'zhangsan', 'lisi', 'my', null, '2023-04-25 09:07:33');
INSERT INTO `chat_records` VALUES ('1650668206723620865', 'zhangsan', 'lisi', 'iiii', null, '2023-04-25 09:09:03');
INSERT INTO `chat_records` VALUES ('1650673593816342529', 'zhangsan', 'lisi', 'nihao', null, '2023-04-25 09:30:27');
INSERT INTO `chat_records` VALUES ('1650674308840361985', 'zhangsan', 'lisi', 'hello,lisi', null, '2023-04-25 09:33:18');
INSERT INTO `chat_records` VALUES ('1650674361378213890', 'lisi', 'zhangsan', 'hhhhhhhh', null, '2023-04-25 09:33:30');

-- ----------------------------
-- Table structure for friends
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` bigint NOT NULL COMMENT '好友表',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '以我的维度(可重复)',
  `friends_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '好友用户名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of friends
-- ----------------------------
INSERT INTO `friends` VALUES ('1', 'zhangsan', 'lisi');
INSERT INTO `friends` VALUES ('2', 'lisi', 'zhangsan');
INSERT INTO `friends` VALUES ('3', 'zhangsan', 'wangwu');
INSERT INTO `friends` VALUES ('4', 'lisi', 'wangwu');
INSERT INTO `friends` VALUES ('5', 'wangwu', 'zhangsan');
INSERT INTO `friends` VALUES ('6', 'wangwu', 'lisi');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint NOT NULL,
  `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '真实姓名',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '昵称',
  `birthday` date DEFAULT NULL COMMENT '出生日期',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '头像',
  `sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '签名',
  `sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '性别',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '账号，类似于QQ号码',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '张三', '张三', '2023-04-20', null, '纸上得来终觉浅，绝知此事要躬行', '1', 'zhangsan', 'zhangsan');
INSERT INTO `user` VALUES ('2', '李四', '李四', '2023-04-22', null, '啊啊啊啊啊啊啊啊', '0', 'lisi', 'lisi');
INSERT INTO `user` VALUES ('3', '王五', '王五', '2023-04-20', null, '8888888888', '1', 'wangwu', 'wangwu');
