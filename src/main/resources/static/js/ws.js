function connect() {
    let host = "ws://localhost:8081/ws/chat"
    socket = new WebSocket(host);
    try {

        socket.onopen = function (msg) {
            console.log("连接成功", msg)
        }
        socket.onmessage = function (msg) {
            console.log("后端发送消息", msg.data)
        };
        socket.onclose = function (msg) {
            console.log("关闭")
        };
    } catch (ex) {
        console.log("异常", ex);
    }
}

function send(msg) {
    socket.send("hello msg!");
}

window.onload = function () {
    if (window.WebSocket != undefined) {
        console.log("支持websocket")
        connect()
    } else {
        console.log("不支持websocket")
    }
}
window.onbeforeunload = function () {
    try {
        socket.close();
        socket = null;
    } catch (ex) {
    }
};