package com.york.qq.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@TableName("user")
@Data
public class User {

    private String realName;

    private String nickName;

    private LocalDateTime birthday;

    private String avatar;

    private String sign;

    private String sex;

    private String username;

    private String password;
}
