package com.york.qq.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ChatRecords {

    private Long id;
    private String sender;
    private String receiver;
    private String content;
    private LocalDateTime createTime;

}
