package com.york.qq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.york.qq.entity.User;

public interface IUserService extends IService<User> {
}
