package com.york.qq.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.york.qq.entity.ChatRecords;
import com.york.qq.mapper.ChatRecordsMapper;
import com.york.qq.service.IChatRecordsService;
import org.springframework.stereotype.Service;

@Service
public class ChatRecordsServiceImpl extends ServiceImpl<ChatRecordsMapper, ChatRecords> implements IChatRecordsService {

    
}
