package com.york.qq.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.york.qq.entity.ChatRecords;

public interface IChatRecordsService extends IService<ChatRecords> {
}
