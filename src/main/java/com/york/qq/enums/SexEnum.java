package com.york.qq.enums;

public enum SexEnum {

    MAN("1", "男"),
    WOMAN("0", "女");

    private String key;

    private String value;

    SexEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public static SexEnum getSexEnum(String key) {
        for (SexEnum bean : SexEnum.values()) {
            if (bean.key.equals(key)) {
                return bean;
            }
        }
        return null;
    }
}
