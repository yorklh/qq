package com.york.qq.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.york.qq.common.Result;
import com.york.qq.entity.User;
import com.york.qq.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class LoginController {

    @Autowired
    private IUserService userService;

    //登录接口
    @PostMapping("login")
    public Result<?> login(String username, String password, HttpServletRequest request) {

        QueryWrapper<User> query = Wrappers.query();
        query.eq("username", username);
        query.eq("password", password);
        List<User> list = userService.list(query);
        if (!list.isEmpty() && list.size() == 1) { // 登录成功
            HttpSession session = request.getSession();
            String sessionId = session.getId();
            System.out.println("登录sessionId" + sessionId);
            User u = new User();
            u.setUsername(username);
            u.setPassword(password);
            session.setAttribute(sessionId, u);
            return Result.ok("登录成功", sessionId);
        }

        return Result.ok("登录失败");
    }
}
