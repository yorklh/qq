package com.york.qq.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.york.qq.common.Result;
import com.york.qq.entity.User;
import com.york.qq.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping("list")
    public Result<?> list(Page<User> page, User user) {
        QueryWrapper<User> query = Wrappers.query(user);
        Page<User> pageRes = userService.page(page, query);
        return Result.ok(pageRes);
    }
}
