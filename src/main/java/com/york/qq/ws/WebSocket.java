package com.york.qq.ws;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.york.qq.config.WebSocketConfig;
import com.york.qq.entity.ChatRecords;
import com.york.qq.entity.User;
import com.york.qq.service.IChatRecordsService;
import com.york.qq.util.ApplicationContextUtil;
import com.york.qq.util.JsonFormatUtils;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;

//configurator = ServerEndpointConfigurator.class
@ServerEndpoint(value = "/ws/chat", configurator = WebSocketConfig.class)
@Component
public class WebSocket {


    // 记录当前有多少个用户加入到了聊天室，它是static全局变量。为了多线程安全使用原子变量AtomicInteger
    private static final AtomicInteger connectionIds = new AtomicInteger(0);

    //每个用户用一个CharAnnotation实例来维护，请你注意它是一个全局的static变量，所以用到了线程安全的CopyOnWriteArraySet
    private static final Set<WebSocket> connections = new CopyOnWriteArraySet<>();

    //key=username, value=websocket
    private static final Map<String, WebSocket> maps = new ConcurrentHashMap<>();

    private Session session;

    private HttpSession httpSession;

    public WebSocket() {
    }

    //根据httpSession，得到user对象
    private User getUser() {
        User user = (User) this.httpSession.getAttribute(this.httpSession.getId());
        return user;
    }

    //新连接到达时，Tomcat会创建一个Session，并回调这个函数
    @OnOpen
    public void start(Session session, EndpointConfig config) {
        HttpSession httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());
        this.httpSession = httpSession;
        User u = getUser();
        this.session = session;
        connections.add(this);
        maps.put(u.getUsername(), this);
    }

    //浏览器关闭连接时，Tomcat会回调这个函数
    @OnClose
    public void end() {
        System.out.println("end==========");
        connections.remove(this);
        User u = new User();
        maps.remove(u.getUsername());

    }

    //浏览器发送消息到服务器时，Tomcat会回调这个函数
    @OnMessage
    public void incoming(String message) {

        //向前端发送数据
        try {
            ChatRecords bean = toChatRecords(message);
            // TODO 保存到数据库
            IChatRecordsService iChatRecordsService = ApplicationContextUtil.getBean(IChatRecordsService.class);
            iChatRecordsService.save(bean);
            // TODO 发送给对应的人
            String s = JsonFormatUtils.writeValueAsString(bean);

            WebSocket webSocket = this.maps.get(bean.getReceiver());
            if (webSocket != null) {
                webSocket.session.getBasicRemote().sendText(s);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    //转换成聊天记录对象
    private ChatRecords toChatRecords(String message) {
        User user = getUser();
        ChatRecords bean = JsonFormatUtils.readValue(message, ChatRecords.class);//objectMapper.readValue(message, ChatRecords.class);
        bean.setSender(user.getUsername());
        bean.setCreateTime(LocalDateTime.now());
        System.out.println(bean);
        return bean;
    }

    @Data
    private static class Tmp {
        private String sender; //发信人
        private String receiver; //接收人
        private String content;
        private String sessionId;
    }

    // WebSocket连接出错时，Tomcat会回调这个函数
    @OnError
    public void onError(Throwable t) throws Throwable {
        t.printStackTrace();
    }

}