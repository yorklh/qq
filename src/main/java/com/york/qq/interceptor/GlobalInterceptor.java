package com.york.qq.interceptor;

import com.york.qq.common.Result;
import com.york.qq.entity.User;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


@Component
public class GlobalInterceptor extends HandlerInterceptorAdapter {

    //静态资源以及登录接口，直接放行
    public static final String[] uris = new String[]{
            "/login.html",
            "/login",
            "/ws/chat"
    };

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (!filter(request)) { // 进入则表示不是静态资源，前后段分离，此处静态资源实际不存在
            HttpSession session = request.getSession();
            String sessionId = session.getId();
            System.out.println("拦截器 sessionId: " + sessionId);
            if (session.getAttribute(sessionId) == null) {
                //没有登录，写入错误信息
                try {
                    //前后端分离方式，这种方式，不需要重定向页面，由前端自己控制
//                    response.getOutputStream().write("您请登录".getBytes(StandardCharsets.UTF_8));
                    //前后端集成方式
                    response.sendRedirect("/login.html");
                    return false;
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

            } else {
                return true; // 已经登录，直接放行
            }
        }
        return true; //直接过url
    }

    //true,表示直接通过
    private boolean filter(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        for (int i = 0; i < uris.length; i++) {
            if (uris[i].startsWith(requestURI)) {
                return true;
            }
        }
        return false;
    }
}
