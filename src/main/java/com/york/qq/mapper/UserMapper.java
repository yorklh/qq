package com.york.qq.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.york.qq.entity.User;

public interface UserMapper extends BaseMapper<User> {

}