package com.york.qq.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.york.qq.entity.ChatRecords;

public interface ChatRecordsMapper extends BaseMapper<ChatRecords> {
}
